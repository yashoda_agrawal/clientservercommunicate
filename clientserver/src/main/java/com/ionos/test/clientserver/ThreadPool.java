package com.ionos.test.clientserver;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadPool {

	final static Logger logger = LoggerFactory.getLogger(ThreadPool.class);

	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 5; i++) {
			Runnable worker = new Client(" " + i);
			if (logger.isDebugEnabled()) {

			}
			executor.execute(worker);
		}
		executor.shutdown();
		logger.info("Finished all threads");

	}

}