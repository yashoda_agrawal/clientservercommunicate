package com.ionos.test.clientserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Client implements Runnable
{
	final static Logger logger = LoggerFactory.getLogger(Client.class);
	String serverName;
	int port;
	String command;
	public Client(String command){
		serverName = "localhost";
		port = 6060;
		this.command=command;
	}
	public Client(String serverName, int port) {
		this.serverName = serverName;
		this.port = port;
	}

	public void run() {
		try
		{	
			boolean flag[] = new boolean[2];        //indicates which thread is ready to enter its critical section.
			int turn;                              //indicates which thread turn it is to enter into critical section

			if (logger.isDebugEnabled()) {
				logger.debug(Thread.currentThread().getName()+" Start. Command = "+command);
				logger.debug("Connecting to " + serverName + " on port " + port);

			}

			Socket client = new Socket(serverName, port);

			if (logger.isDebugEnabled()) {
				logger.debug("Just connected to " + client.getRemoteSocketAddress());

			}

			//System.out.println("Just connected to " + client.getRemoteSocketAddress());
			OutputStream outToServer = client.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);
			int i=0;
			int j=1;
			flag[i] = true;	
			turn = j;
			while(flag[j] == true && turn == j);
			out.writeUTF("Hello from "+ client.getLocalSocketAddress() + "  Alive");
			out.writeUTF("Client sends=======================>Event1");
			flag[i]=false;
			InputStream inFromServer = client.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);
			
			///////////////////////////////////////////////////////////
			//while (Server.InterCommunicateflag) ;
			/*{
                wait(100);
            }*/
			//Server.InterCommunicateflag=true;
			
			
			
			while (!Server.InterCommunicateflag) ;
			Server.InterCommunicateflag=false;
			////////////////Incoming from server///////////////
			flag[j] = true;	
			turn = i;
			while(flag[i] == true && turn == i);
			System.out.println("Server says " + in.readUTF());
			flag[j]=false;
			///////////////////////////////////////////////////
			
			flag[i] = true;	
			turn = j;
			while(flag[j] == true && turn == j);
			out.writeUTF("Client sends====================>Event3");
			out.writeUTF("Client says=====================>Terminating");
			flag[i]=false;
			
			////////////////////////////////////////////////
			out.close();
			in.close();
			client.close();
			if (logger.isDebugEnabled()) {
				logger.debug(Thread.currentThread().getName()+" End.");

			}
			
			
		//System.out.println(in.readUTF());
		
		//System.out.println(Thread.currentThread().getName()+" End.");

	}
	catch(IOException e)
	{
		e.printStackTrace();
	}
		/*catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
}

}